#include "Accessory.h"

Accessory::Accessory(std::string accessoryName, std::string description)
{
  this->accessoryName = accessoryName;
  this->description = description;
};

std::string Accessory::getName()
{
  return accessoryName;
}

std::string Accessory::getDescription()
{
  return description;
}

Shield::Shield():Accessory::Accessory("Shield", "This is a shield. It blocks stuff")
{

};

Pendant::Pendant():Accessory::Accessory("Pendant", "A shiny pendant. Probably magical or something")
{

};

Potion::Potion():Accessory::Accessory("Potion", "Makes you feel better!")
{

};
