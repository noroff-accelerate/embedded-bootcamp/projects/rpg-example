#include "Character.h"

Character::Character(std::string characterClassName, int bodyScore, int speedScore, int knowledgeScore)
{
  this->characterClassName = characterClassName;
  this->bodyScore = bodyScore;
  this->speedScore = speedScore;
  this->knowledgeScore = knowledgeScore;
};

int Character::getAttack(){
  return bodyScore + weapons.front().attackDamage;
}

std::string Character::getName()
{
  return characterClassName;
}

void Character::addWeapon(Weapon weapon)
{
  weapons.push_back(weapon);
}

std::vector<Weapon> Character::listWeapons()
{
  return weapons;
}

void Character::addAccessory(Accessory accessory)
{
  accesories.push_back(accessory);
}

std::vector<Accessory> Character::listAccessories()
{
  return accesories;
}

Warrior::Warrior():Character::Character("Warrior", 15, 12, 10)
{

};

Mage::Mage():Character::Character("Mage", 10, 10, 15)
{

};

Ranger::Ranger():Character::Character("Ranger", 12, 12, 15)
{

};

Rogue::Rogue():Character::Character("Rogue", 10, 15, 15)
{

};
