#pragma once

#include "Weapon.h"
#include "Accessory.h"

#include <string>
#include <vector>

class Character
{
  protected:
    std::string characterClassName;
    int bodyScore;
    int speedScore;
    int knowledgeScore;
    std::vector<Weapon> weapons;
    std::vector<Accessory> accesories;
  public:
    Character(std::string characterClassName, int bodyScore, int speedScore, int knowledgeScore);

    std::string getName();

    void addWeapon(Weapon weapon);
    std::vector<Weapon> listWeapons();

    void addAccessory(Accessory Accessory);
    std::vector<Accessory> listAccessories();

    int getAttack();
};

class Warrior: public Character
{
  public:
    Warrior();
};

class Mage: public Character
{
  public:
    Mage();
};

class Rogue: public Character
{
  public:
    Rogue();
};

class Ranger: public Character
{
  public:
    Ranger();
};
