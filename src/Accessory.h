#pragma once

#include <string>

class Accessory
{
  protected:
    std::string accessoryName;
    std::string description;
  public:
    Accessory(std::string accessoryName, std::string description);
    std::string getName();
    std::string getDescription();
};

class Shield: public Accessory
{
  public:
    Shield();
};

class Pendant: public Accessory
{
  public:
    Pendant();
};

class Potion: public Accessory
{
  public:
    Potion();
};
