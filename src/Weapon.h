#pragma once

#include <string>

class Weapon
{
  protected:
    std::string weaponName;
    //int attackDamage;
    int attackSpeed;
    bool ranged;
  public:
    Weapon(std::string weaponName, int attackDamage, int attackSpeed, bool ranged);
    std::string getName();
    int attackDamage;
};

class Sword: public Weapon
{
  // sword specific stuff
  public:
    Sword();
    Sword(std::string);
};

class Dagger: public Weapon
{
  public:
    Dagger();
};

class Bow: public Weapon
{
  public:
    Bow();
};

class Staff: public Weapon
{
  public:
    Staff();
    void thonk();
};
