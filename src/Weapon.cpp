#include "Weapon.h"

#include <iostream>

Weapon::Weapon(std::string weaponName, int attackDamage, int attackSpeed, bool ranged)
{
  this->weaponName = weaponName;
  this->attackDamage = attackDamage;
  this->attackSpeed = attackSpeed;
  this->ranged = ranged;
};

std::string Weapon::getName()
{
  return weaponName;
}

Sword::Sword(std::string name = "Sword"):Weapon::Weapon(name, 150, 100, false)
{
  std::cout << "new sword" << std::endl;
  // sword specific stuff
};

Dagger::Dagger():Weapon::Weapon("Dagger", 101, 150, false)
{
  std::cout << "new dagger" << std::endl;
};

Bow::Bow():Weapon::Weapon("Bow", 120, 120, true)
{
  std::cout << "new bow" << std::endl;
};

Staff::Staff():Weapon::Weapon("Staff", 99, 99, false)
{
  std::cout << "new staff" << std::endl;
};

void Staff::thonk()
{
  std::cout << "Staff goes thonk" << std::endl;
}
