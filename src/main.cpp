#include "Accessory.h"
#include "Character.h"
#include "Weapon.h"

#include <iostream>

int main() {

/*
  Character firstChar = Warrior();
  Character secondChar = Mage();
  Character thirdChar = Rogue();
  Character forthChar = Ranger();

  Weapon sword = Sword();
  Weapon dagger = Dagger();
  Weapon bow = Bow();
  Weapon staff = Staff();

  Accessory shield = Shield();
  Accessory pendant = Pendant();
  Accessory potion = Potion();
  */
  /* code */

  Character test = Mage();
  test.addWeapon(Dagger());
  test.addAccessory(Potion());

  std::cout << "Character Class Name: " << test.getName() <<std::endl;

  for (Weapon w: test.listWeapons()) {
    std::cout << "Weapon Name: " << w.getName() <<std::endl;
    std::cout << "Weapon damage: " << w.attackDamage <<std::endl;

    // OMG THAT WAS MUCH EASIER THAN EXPECTED
    Staff* s = (Staff*)&w;
    s->thonk();
  }

  for (Accessory acc: test.listAccessories()) {
    std::cout << "Accessory Name: " << acc.getName() <<std::endl;
    std::cout << "Description: " << acc.getDescription() <<std::endl;
  }

  std::cout << "Attack Strength: " << std::to_string(test.getAttack()) <<std::endl;

  return 0;
}
