# RPG Character Builder

A simple example of classes for build RPG characters.

## Usage

```
mkdir build && cd build
cmake ../
make
./main
```

## Author

Craig Marais (@muskatel)
